const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2018509", "root", "root", {
    host: "localhost",
    dialect: "mysql",
    define: {
        timestamps: false
    }
});

const db={};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;
 
db.Aktivnost = require(__dirname+"/aktivnost.js")(sequelize, Sequelize.DataTypes)
db.Dan = require(__dirname+"/dan.js")(sequelize, Sequelize.DataTypes)
db.Grupa = require(__dirname+"/grupa.js")(sequelize, Sequelize.DataTypes)
db.Predmet = require(__dirname+"/predmet.js")(sequelize, Sequelize.DataTypes)
db.Student = require(__dirname+"/student.js")(sequelize, Sequelize.DataTypes)
db.Tip = require(__dirname+"/tip.js")(sequelize, Sequelize.DataTypes)
 
db.Predmet.hasMany(db.Grupa);
db.Grupa.belongsTo(db.Predmet);
 
db.Predmet.hasMany(db.Aktivnost);
db.Aktivnost.belongsTo(db.Predmet);

db.Grupa.hasMany(db.Aktivnost);
db.Aktivnost.belongsTo(db.Grupa);
 
db.Dan.hasMany(db.Aktivnost);
db.Aktivnost.belongsTo(db.Dan);
 
db.Tip.hasMany(db.Aktivnost);
db.Aktivnost.belongsTo(db.Tip);
 
db.Student.belongsToMany(db.Grupa, { as: 'grupe', through: 'studentgrupa', foreignKey: 'studentId' });
db.Grupa.belongsToMany(db.Student, { as: 'studenti', through: 'studentgrupa', foreignKey: 'grupaId' });
 
module.exports=db;