const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');
const app = express();

app.use(express.static('public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const Sequelize = require('sequelize')
const sequelize = require('./db.js')
const db = require('./db.js')
 
db.sequelize
    .sync()
    .then(() => {
        console.log("Gotovo kreiranje tabela!");
    });
 
app.get("/v2/predmet", function(req, res){
  res.setHeader("Access-Control-Allow-Origin", "*");
    db.Predmet.findAll()
        .then(predmeti => res.json(predmeti))
        .catch(() =>
            res.json({message: "Greška prilikom dohvaćanja predmeta!"})
        )

})

app.get("/v2/predmet/:id", function(req, res){
  db.Predmet.findOne({
    where : {
      id : req.params.id
    }
  }).then(function(predmet){
    if(predmet){
      res.json(predmet)
    }
    else{
      res.json({message : "Predmet sa id " + req.params.id + " ne postoji!"})
    }
  }).catch(() => res.json({message : "Greška prilikom dohvaćanja predmeta!"}))
})

app.post("/v2/predmet", function(req, res){
    db.Predmet.findOrCreate({
      where : {naziv : req.body.naziv.toUpperCase()},
      defaults : {
        naziv : req.body.naziv.toUpperCase()
      }
    })
        .then(function(povratna){
          if(povratna[1])
            res.json({
                message: "Uspješno dodan predmet!",
                predmet: povratna[0]
            })
            else {
              res.json({
              message: "Naziv predmeta postoji!",
              predmet: povratna[0]
          })
        }
          }).catch(() =>
            res.json({message: "Dodavanje predmeta neuspješno!"})
        )
})
app.put("/v2/predmet/:id", function(req, res){
    db.Predmet.update(
        req.body,
        {
            where: {
                id: Number(req.params.id)
            }
        })
        .then(function(predmet){
          if(predmet==1)
            res.json({
                message: "Uspješno izmijenjen predmet!",
                predmet: req.body
            })
            else{
              res.json({
                message: "Nema promjene!"
            })
            }
          })
        .catch(() =>
            res.json({message: "Predmet nije uspješno izmijenjen!"})
        )
})
app.delete("/v2/predmet/:id", function(req, res){
    db.Predmet.destroy({
        where: {
            id: Number(req.params.id)
        }
    })
        .then(() => {
            res.json({message: "Uspješno obrisan predmet!"})
        })
        .catch(() =>
            res.json({message: "Greška - predmet nije obrisan!"})
        )
})

app.get("/v2/dan", function(req, res){
  db.Dan.findAll()
      .then(function(dani) {
        res.json(dani)
      }).catch(() =>
          res.json({message: "Greška prilikom dohvaćanja dana!"})
      )
})

app.get("/v2/dan/:id", function(req, res){
  db.Dan.findOne({
    where : {
      id : req.params.id
    }
  }).then(function(dan){
    if(dan){
      res.json(dan)
    }
    else{
      res.json({message : "Dan sa id " + req.params.id + " ne postoji!"})
    }
  }).catch(() => res.json({message : "Greška prilikom dohvaćanja dana!"}))
})

app.post("/v2/dan", function(req, res){
  db.Dan.findOrCreate({
    where : {naziv : req.body.naziv.toUpperCase()},
    defaults : {
      naziv : req.body.naziv.toUpperCase()
    }
  })
      .then(function(povratna){
        if(povratna[1])
          res.json({
              message: "Uspješno dodan dan!",
              dan: povratna[0]
          })
          else {
            res.json({
            message: "Naziv dana postoji!",
            dan: povratna[0]
        })
      }
        }).catch(() =>
          res.json({message: "Dodavanje dana neuspješno!"})
      )
})

app.put("/v2/dan/:id", function(req, res){
  db.Dan.update(
      req.body,
      {
          where: {
              id: Number(req.params.id)
          }
      })
      .then(() =>
          res.json({
              message: "Uspješno izmijenjen dan!"
          }))
      .catch(() =>
          res.json({message: "Dan nije uspješno izmijenjen!"})
      )
})

app.delete("/v2/dan/:id", function(req, res){
  db.Dan.destroy({
      where: {
          id: Number(req.params.id)
      }
  })
      .then(() => {
          res.json({message: "Uspješno obrisan dan!"})
      })
      .catch(() =>
          res.json({message: "Greška - dan nije obrisan!"})
      )
})

app.get("/v2/tip", function(req, res){
  res.setHeader("Access-Control-Allow-Origin", "*");
  db.Tip.findAll()
      .then(function(tipovi){
        res.json(tipovi)
      }).catch(() =>
          res.json({message: "Greška prilikom dohvaćanja tipova!"})
      )
})

app.get("/v2/tip/:id", function(req, res){
  db.Tip.findOne({
    where : {
      id : req.params.id
    }
  }).then(function(tip){
    if(tip){
      res.json(tip)
    }
    else{
      res.json({message : "Tip sa id " + req.params.id + " ne postoji!"})
    }
  }).catch(() => res.json({message : "Greška prilikom dohvaćanja tipa!"}))
})


app.post("/v2/tip", function(req, res){
  db.Tip.findOrCreate({
    where : {naziv : req.body.naziv.toUpperCase()},
    defaults : {
      naziv : req.body.naziv.toUpperCase()
    }
  })
      .then(function(povratna){
        if(povratna[1])
          res.json({
              message: "Uspješno dodan tip!",
              tip: povratna[0]
          })
          else {
            res.json({
            message: "Naziv tipa postoji!",
            tip: povratna[0]
        })
      }
        }).catch(() =>
          res.json({message: "Dodavanje tipa neuspješno!"})
      )
})

app.put("/v2/tip/:id", function(req, res){
  db.Tip.update(
      req.body.naziv.toUpperCase(),
      {
          where: {
              id: Number(req.params.id)
          }
      })
      .then(() =>
          res.json({
              message: "Uspješno izmijenjen tip!"
          }))
      .catch(() =>
          res.json({message: "Tip nije uspješno izmijenjen!"})
      )
})

app.delete("/v2/tip/:id", function(req, res){
  db.Tip.destroy({
      where: {
          id: Number(req.params.id)
      }
  })
      .then(() => {
          res.json({message: "Uspješno obrisan tip!"})
      })
      .catch(() =>
          res.json({message: "Greška - tip nije uspješno obrisan!"})
      )
})


app.get("/v2/grupa", function(req, res){
  db.Grupa.findAll({
    include : {
      model: db.Predmet,
      attributes: ['id', 'naziv']
    },
    attributes : ['id', 'naziv']
  })
      .then(function(grupe){
        res.json(grupe)
      }).catch(() =>
          res.json({message: "Greška prilikom dohvaćanja grupa!"})
      )
})

app.get("/v2/grupa/:id", function(req, res){
  db.Grupa.findOne({
    where : {
      id : req.params.id
    },
    include: {
      model: db.Predmet,
      attributes: ['id', 'naziv']
    },
    attributes: ['id', 'naziv']
  }).then(function(grupa){
    if(grupa)
      res.json(grupa);
    else 
      res.json({message : "Ne postoji grupa sa id " + req.params.id + "!"})
  }).catch(() => res.json({message : "Greška prilikom dohvaćanja grupe!"}))
})

app.post("/v2/grupa", function(req, res){
  db.Grupa.create(req.body)
      .then(() =>
          res.json({
              message: "Uspješno dodana grupa!"
          }))
      .catch(() =>
          res.json({message: "Grupa nije uspješno dodana!"})
      )
})

app.put("/v2/grupa/:id", function(req, res){
  db.Grupa.update(
      req.body,
      {
          where: {
              id: Number(req.params.id)
          }
      })
      .then(() =>
          res.json({
              message: "Uspješno izmijenjena grupa!"
          }))
      .catch(() =>
          res.json({message: "Grupa nije uspješno izmijenjena!"})
      )
})

app.delete("/v2/grupa/:id", function(req, res){
  db.Grupa.destroy({
      where: {
          id: Number(req.params.id)
      }
  })
      .then(() => {
          res.json({message: "Uspješno obrisana grupa!"})
      })
      .catch(() =>
          res.json({message: "Greška - grupa nije obrisana!"})
      )
})

app.get("/v2/student", function(req, res){
  db.Student.findAll()
      .then(function(studenti){
        res.json(studenti)
      }).catch(() =>
          res.json({message: "Greška prilikom dohvaćanja studenata!"})
      )
})

app.get("/v2/student/:id", function(req, res){
  db.Student.findOne({
    where : {
      id : req.params.id
    }
  }).then(function(student){
    if(student)
      res.json(student);
    else 
      res.json({message : "Ne postoji student sa id " + req.params.id + "!"})
  }).catch(() => res.json({message : "Greška prilikom dohvaćanja studenta!"}))
})

app.post("/v2/studenti", function(req, res){
  var studenti = req.body;
  db.Student.findAll().then(function(svi){
    let msg = [];
    for(var jedan of studenti){
      db.Student.findOne({
        where : {
          ime : jedan.ime,
          index : jedan.index
        }
      }).then(function(taj){
        if(taj){
          taj.getGrupe().then(rezultat => 
            rezultat.map(red => red.dataValues)
          ).then(function(grupe){
            db.Grupa.findOne({
              where : {
                id : jedan.grupaId
              }
            }).then(function(prijasnja){
              var predmetID = prijasnja.dataValues.predmetId;
              grupe.forEach(grupa => {
                if(grupa.predmetId == predmetID)
                  taj.removeGrupe([grupa.id]);
              })
              taj.addGrupe([jedan.grupaId])
            })
          })
        }
      })
      if(svi.every(student => student.ime != jedan.ime && student.index != jedan.index)){
        db.Student.create(jedan).then(function(st){
          st.setGrupe([jedan.grupaId])
        })
      }
      else{
        var istiIndex = svi.find(st => st.index == jedan.index && st.ime != jedan.ime)
        if(istiIndex) msg.push('Student '+ jedan.ime + ' nije kreiran jer postoji student ' + istiIndex.ime + ' sa istim indexom ' + jedan.index )
      }
    }
    res.json({ message : msg})
  })
})

app.post("/v2/student", function(req, res){

    db.Student.findOrCreate({
      where : {
        index : req.body.index
      },
      defaults : req.body
    }).then(function(povratna){
      var kreiran = povratna[1]
      if(kreiran) {
        res.json( {
        message : "Uspješno dodan student!",
        student : povratna[0]
      })
    }
      else {
        res.status(409)
        res.json({
          message : 'Student '+ req.body.ime + ' nije kreiran jer postoji student ' + povratna[0].ime + ' sa istim indexom ' + req.body.index ,
          student : povratna[0]
        })
      }
    }).catch(() => res.json({message : "Dodavanje studenta nije uspješno!"}))
        
})

app.put("/v2/student/:id", function(req, res){
  if(req.body.index){
    db.Student.findAll({
      where : {
        index : req.body.index
      }
    }).then(function(studenti){
      if(studenti.length !=0) {
        res.json({message: "Student nije uspješno izmijenjen, jer postoji student sa istim brojem indexa!"})
      }
      else{
        db.Student.update(
        req.body,
        {
            where: {
                id: Number(req.params.id)
            }
        })
        .then(() =>
            res.json({
                message: "Uspješno izmijenjen student!"
            }))
        .catch(() =>
            res.json({message: "Student nije uspješno izmijenjen!"})
        )
      }
    })
  }
  else{
    db.Student.update(
      req.body,
      {
          where: {
              id: Number(req.params.id)
          }
      })
      .then(() =>
          res.json({
              message: "Uspješno izmijenjen student!"
          }))
      .catch(() =>
          res.json({message: "Student nije uspješno izmijenjen!"})
      )
  }
    
  
})

app.delete("/v2/student/:id", function(req, res){
  db.Student.destroy({
      where: {
          id: Number(req.params.id)
      }
  })
      .then(() => {
          res.json({message: "Uspješno obrisan student!"})
      })
      .catch(() =>
          res.json({message: "Greška - student nije obrisan!"})
      )
})

app.get("/v2/aktivnost", function(req, res){
  res.setHeader("Access-Control-Allow-Origin", "*");
  db.Aktivnost.findAll({
    include : 
      [{ model : db.Dan},
        { model : db.Tip},
        { model : db.Grupa},
        { model : db.Predmet}],
    attributes : ['id', 'naziv', 'pocetak', 'kraj']
  })
      .then(function(aktivnosti){
        res.json(aktivnosti)
      }).catch(() =>
          res.json({message: "Greška prilikom dohvaćanja aktivnosti!"})
      )
})

app.get("/v2/aktivnost/:id", function(req, res){
  db.Aktivnost.findOne({
    where : {
      id : req.params.id
    },
    include : 
      [{ model : db.Dan},
        { model : db.Tip},
        { model : db.Grupa},
        { model : db.Predmet}],
    attributes : ['id', 'naziv', 'pocetak', 'kraj']
  }).then(function(aktivnost){
    if(aktivnost)
      res.json(aktivnost);
    else 
      res.json({message : "Ne postoji aktivnost sa id " + req.params.id + "!"})
  }).catch(() => res.json({message : "Greška prilikom dohvaćanja aktivnosti!"}))
})

function jeLiValidna(aktivnost, aktivnosti){
  aktivnost.pocetak  = Number(aktivnost['pocetak']);
  aktivnost.kraj = Number(aktivnost['kraj']);

  let upisi = true;
  if(aktivnost.pocetak>aktivnost.kraj || aktivnost.pocetak%0.5!=0 || aktivnost.kraj%0.5!=0 || aktivnost.kraj > 20 || aktivnost.pocetak<8 || aktivnost.naziv==""){
    upisi = false;
  }
  else{
      for(var i=0; i< aktivnosti.length; i++){
        if(aktivnosti[i].DanId==aktivnost.DanId){
          var pocetak = Number(aktivnosti[i].pocetak);
          var kraj = Number(aktivnosti[i].kraj);
          
          if((aktivnost.pocetak>= pocetak && aktivnost.kraj <=kraj) || (aktivnost.pocetak < pocetak && aktivnost.kraj > kraj) || (aktivnost.pocetak<=pocetak && aktivnost.kraj> pocetak && aktivnost.kraj<= kraj) || (aktivnost.pocetak>= pocetak && aktivnost.pocetak< kraj && aktivnost.kraj>=kraj)){
            upisi = false;
          }
        }
      }
      
      }
      return upisi;
  }


app.post("/v2/aktivnost", (req, res) => {
  db.Aktivnost.findAll()
      .then(function(aktivnosti) {
          if(jeLiValidna(req.body, aktivnosti)) {
              db.Aktivnost.create(req.body)
                  .then(() =>
                      res.json({
                          message: "Uspješno dodana aktivnost!",
                          aktivnost: req.body
                      }))
                  .catch(() =>
                      res.json({message: "Aktivnost nije uspješno dodana!"})
                  )
          }
          else{
              res.json({message: "Aktivnost nije validna!"})
          }
      })
})

app.put("/v2/aktivnost/:id", function(req, res){
  db.Aktivnost.update(
      req.body,
      {
          where: {
              id: Number(req.params.id)
          }
      })
      .then(() =>
          res.json({
              message: "Uspješno izmijenjena aktivnost!"
          }))
      .catch(() =>
          res.json({message: "Aktivnost nije uspješno izmijenjena!"})
      )
})

app.delete("/v2/aktivnost/:id", function(req, res){
  db.Aktivnost.destroy({
      where: {
          id: Number(req.params.id)
      }
  })
      .then(() => {
          res.json({message: "Uspješno obrisana aktivnost!"})
      })
      .catch(() =>
          res.json({message: "Greška - aktivnost nije obrisana!"})
      )
})


  app.get('/v1/predmeti', function(req,res){
    fs.readFile('predmeti.txt', function (err, data) {
      if (err) {
        throw err;
      }
      res.writeHead(200, {'Content-Type': 'application/json'});
      var redovi = data.toString().split(/[\n\r]+/g);
      var tekst='[' ;
      for(i=0;i<redovi.length-1; i++){
        tekst+= ' {"naziv":"' + redovi[i]+ '"}' ;
        if(i!= redovi.length - 2) tekst+=',';
      }
      tekst+=' ]';
      res.end(tekst);
    });
  });

  app.get('/v1/aktivnosti', function(req,res){
    
    fs.readFile('aktivnosti.txt', function (err, data) {
      if (err) {
        throw err;
      }
      res.writeHead(200, {'Content-Type': 'application/json'});   
      var redovi = data.toString().split(/[\n\r]+/g);
      var tekst='[' ;
      tekst+= '\n';
      for(var i=0; i< redovi.length -1 ; i++){
        tekst+='{ ';
        var kolone= redovi[i].split(',');
        for(var j=0; j<kolone.length; j++){
          if(j==0)tekst+='"naziv":"' + kolone[j]+ '",';
          else if(j==1)tekst+='"tip":"' + kolone[j]+ '",';
          else if(j==2)tekst+='"pocetak":' + kolone[j]+ ',';
          else if(j==3)tekst+='"kraj":' + kolone[j]+ ',';
          else if(j==4)tekst+='"dan":"' + kolone[j] + '"';
        }
        tekst+= ' }';
        if(i!= redovi.length - 2) tekst+=',';
        tekst+= '\n';
      }
      tekst+=' ]';
      res.end(tekst);
    });
  });

app.get('/v1/predmet/:naziv/aktivnost/', function(req,res){
  fs.readFile('aktivnosti.txt', function (err, data) {
    if (err) {
      throw err;
    }
    res.writeHead(200, {'Content-Type': 'application/json'});
    var redovi = data.toString().split(/[\n\r]+/g);
    var naziv = req.params.naziv;
    var tekst='[' ;
    tekst+= '\n';
    var brojac = 0;
    for(var i=0; i< redovi.length-1; i++){
      var kolone= redovi[i].split(',');
      if(kolone[0].toLowerCase() == naziv.toLowerCase()){
        brojac=brojac+1;
      }
    }
    var b=0;
    for(var i=0; i< redovi.length -1; i++){
      var kolone= redovi[i].split(',');
      if(kolone[0].toLowerCase() == naziv.toLowerCase()){
        b=b+1;
        tekst+='{ ';
        for(var j=0; j<kolone.length; j++){
          if(j==0)tekst+='"naziv":"' + kolone[j]+ '",';
          else if(j==1)tekst+='"tip":"' + kolone[j]+ '",';
          else if(j==2)tekst+='"pocetak":' + kolone[j]+ ',';
          else if(j==3)tekst+='"kraj":' + kolone[j]+ ',';
          else if(j==4)tekst+='"dan":"' + kolone[j] + '"';
        }
        tekst+= ' }';
        if(b<brojac) tekst+=',';
        tekst+= '\n';
      }
    }
    tekst+=' ]';
    res.end(tekst);
  });
});

app.post('/v1/predmet',function(req,res){
  let tijelo = req.body;
  let naziv =tijelo['naziv'];
  let tekst='';
  fs.readFile('predmeti.txt', function (err, data) {
    if (err) {
        throw err;
     }
     
    var redovi = data.toString().split(/[\n\r]+/g);
     
    for(i=0;i<redovi.length - 1; i++){
      if(naziv.toLowerCase() == redovi[i].toLowerCase()) {
        tekst={message:"Naziv predmeta postoji!"};
      }
    }
    if(tekst==''){
      let novaLinija =  naziv + "\n";
      fs.appendFile('predmeti.txt',novaLinija,function(err){
        if(err) throw err;
      });
      tekst={message:"Uspješno dodan predmet!"};
     }
     res.json(tekst);   
  });
});

app.post('/v1/aktivnost',function(req,res){
  let tijelo = req.body;
  tijelo.pocetak  = Number(tijelo['pocetak']);
  tijelo.kraj = Number(tijelo['kraj']);

  const aktivnost = tijelo
  var dani = ["ponedjeljak", "utorak", "srijeda", "četvrtak", "petak", "subota", "nedjelja"]
  let upisi = true;
  if(aktivnost.pocetak>aktivnost.kraj || aktivnost.pocetak%0.5!=0 || aktivnost.kraj%0.5!=0 || aktivnost.kraj > 20 || aktivnost.pocetak<8 || !dani.includes(aktivnost.dan.toLowerCase()) || aktivnost.naziv==""){
    res.json({message:"Aktivnost nije validna!"})
    upisi = false;
  }
  else{
    fs.readFile('aktivnosti.txt', function (err, data) {
      if (err) {
        throw err;
      }
      var tekst= ''
      var redovi = data.toString().split(/[\n\r]+/g);
      for(var i=0; i< redovi.length -1; i++){
        var kolone= redovi[i].split(',');
        if(kolone[4].toLowerCase()==aktivnost.dan.toLowerCase()){
          var pocetak = Number(kolone[2]);
          var kraj = Number(kolone[3]);
          
          if((aktivnost.pocetak>= pocetak && aktivnost.kraj <=kraj) || (aktivnost.pocetak < pocetak && aktivnost.kraj > kraj) || (aktivnost.pocetak<=pocetak && aktivnost.kraj> pocetak && aktivnost.kraj<= kraj) || (aktivnost.pocetak>= pocetak && aktivnost.pocetak< kraj && aktivnost.kraj>=kraj)){
            upisi = false;
            tekst = {message:"Aktivnost nije validna!"}
          }
        }
      }
      if(!upisi) res.json(tekst);
      else{
        let novaLinija = tijelo["naziv"]+',' + tijelo["tip"] + ',' + tijelo["pocetak"] + ',' + tijelo["kraj"] + ',' + tijelo["dan"] + "\n";
        fs.appendFile('aktivnosti.txt',novaLinija,function(err){
          if(err) res.json({message:"Aktivnost nije validna!"});
          else res.json({message:"Uspješno dodana aktivnost!"});
        });
      }
    });
  }
});

app.delete('/v1/all', function(req,res){
  let ok= true;

  fs.writeFile('predmeti.txt', '', function(err){
    if (err) {
      res.json({message:"Greška - sadržaj datoteka nije moguće obrisati!"})
      ok=false;
    }
    if(ok){
      fs.writeFile('aktivnosti.txt', '', function(err){
        if (err) {
          res.json({message:"Greška - sadržaj datoteka nije moguće obrisati!"})
        }
        else {
          res.json({message:"Uspješno obrisan sadržaj datoteka!"})
        }
      })
    }
  })
})

app.delete('/v1/predmet/:naziv', function(req,res){
  var naziv = req.params.naziv;

  fs.readFile('predmeti.txt', function (err, data) {
    if (err) {
      res.json({message:"Greška - predmet nije obrisan!"})
    }
    else {
      var redovi = data.toString().split(/[\n\r]+/g);
      var dodati='';
      var ima=false;
      for(var i=0; i< redovi.length-1; i++){
        if(redovi[i]!='' && redovi[i]!=naziv ){
          dodati= dodati + redovi[i] + '\n';
        }
        if(redovi[i]==naziv){
          ima=true;
        }
      }
      if(ima){
        fs.writeFile('predmeti.txt', dodati, function (err) {
          if (err) {
            res.json({message:"Greška - predmet nije obrisan!"})
          }
          else{
            res.json( {message:"Uspješno obrisan predmet!"});
          }
        });
      }
      else{
        res.json({message:"Greška - predmet nije obrisan!"})
      }
    }
  })  
});

app.delete('/v1/aktivnost/:naziv', function(req,res){
  var naziv = req.params.naziv;

  fs.readFile('aktivnosti.txt', function (err, data) {
    if (err) {
      res.json({message:"Greška - aktivnost nije obrisana!"})
    }
    else {
      var redovi = data.toString().split(/[\n\r]+/g);
      var dodati='';
      var ima=false;
      for(var i=0; i< redovi.length; i++){
        if(redovi[i]!=''){
          var kolone= redovi[i].split(',');
          if(kolone[0]!=naziv){
            dodati= dodati + redovi[i] + '\n';
          }
          else{
            ima=true;
          }
        }
      }

      if(ima){
        fs.writeFile('aktivnosti.txt', dodati, function (err) {
          if (err) {
            res.json({message:"Greška - aktivnost nije obrisana!"})
          }
          else{
            res.json( {message:"Uspješno obrisana aktivnost!"});
          }
        })
      }
      else{
        res.json({message:"Greška - aktivnost nije obrisana!"})
      }
    }
  });
});

app.listen(3000);

module.exports=app;