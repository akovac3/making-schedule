let chai = require("chai");
let server = require("./skripta")
const fs = require('fs');
const assert = require("assert");
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const { response } = require("express");

chai.should();

let podaci = fs.readFileSync('testniPodaci.txt', 'utf8');

describe('Testiranje', () => {
    let testovi = []
    podaci = podaci.toString()
    podaci = podaci.replace(/\\/gi, "").split(/[\n\r]+/g)
 
    podaci.forEach(red => {
        let test = {}
        let nizTestova = red.split(",")
    
        test.operacija = nizTestova[0]
        test.ruta = nizTestova[1]
        
        let aktivnost = ""
        if(red.startsWith("GET,/v1/predmet") || red.startsWith("GET,/v1/aktivnosti")){
            for(let i = 3; i < nizTestova.length; i++){
                aktivnost += nizTestova[i]
                if(i !== nizTestova.length - 1)aktivnost += ","
            }
            test.ulaz = JSON.parse(nizTestova[2])
            test.izlaz = JSON.parse(aktivnost)
        }
        else if(red.startsWith("POST,/v1/aktivnost")) {
            for (let i = 2; i < nizTestova.length - 1; i++) {
                aktivnost += nizTestova[i]
                if(i !== nizTestova.length - 2)aktivnost += ","
            }

            test.ulaz = JSON.parse(aktivnost)
            test.izlaz = JSON.parse(nizTestova[nizTestova.length - 1])
        }
        
        else{
            var ulaz = JSON.parse(nizTestova[2])
            test.ulaz = ulaz
            test.izlaz = JSON.parse(nizTestova[3])
        }

        testovi.push(test)

        if(test.operacija =='POST'){
            it('Test ' + test.operacija + test.ruta, function(done){
                chai.request(server).post(test.ruta).send(test.ulaz).end(function(err, res){
                    res.body.should.be.eql(test.izlaz);
                    done();
                })
            })
        }
        if(test.operacija =='GET'){
            it('Test ' + test.operacija +test.ruta, function(done){
                chai.request(server).get(test.ruta).end(function(err, res){
                    res.body.should.be.eql( test.izlaz);
                    done();
                })
            })
        }
        if(test.operacija =='DELETE'){
            it('Test ' + test.operacija +test.ruta, function(done){
                chai.request(server).delete(test.ruta).end(function(err, res){
                    res.body.should.be.eql(test.izlaz)
                    done();
                })
            })
        }
    })
});