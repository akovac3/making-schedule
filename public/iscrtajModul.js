let Raspored = (function(){
    var iscrtajRaspored = function(div, dani, satPocetak, satKraj) {
      if(satPocetak>=satKraj || !Number.isInteger(satPocetak) || !Number.isInteger(satKraj) || satPocetak<0 || satPocetak>24 || satKraj<0 || satKraj>24){
        var tekst= document.createTextNode("Greška");
        div.appendChild(tekst);
      }
      else {
      var tbl = document.createElement("table");
      tbl.classList.add("tg");
        
      var max= (satKraj - satPocetak)*2 +1;
      var pocetak =satPocetak;
    
      for (var i = 0; i < dani.length+1; i++) {
        var row = document.createElement("tr");
            
        for (var j = 0; j < max; j++) {
          var cell ;
    
          if(i!=0) cell = document.createElement("td");
    
          if(i==0){
            cell = document.createElement("th");
            cell.classList.add("sat"); 
            if(pocetak<=12 && pocetak%2==0 && pocetak<satKraj){
              cell.colSpan="2";
              var tekst;
              if(pocetak<10) tekst= "0"+pocetak+":00";
              else tekst= pocetak+":00";
                var cellText = document.createTextNode(tekst);
                cell.appendChild(cellText);
                pocetak+=1;
                j++;
            }
    
            else if(pocetak>=15 && (pocetak+1)%2==0 && pocetak<satKraj){
              cell.colSpan="2";
              var tekst = pocetak + ":00";
              var cellText = document.createTextNode(tekst);
              cell.appendChild(cellText);
              pocetak+=1;
              j++;
            }
    
            else if(pocetak==satPocetak && ((pocetak%2!=0 && satKraj-satPocetak==1) || (satPocetak==13 && satKraj==15))){
              cell.colSpan="2";
              var tekst;
              if(pocetak<10) tekst= "0"+pocetak+":00";
              else tekst= pocetak+":00";
                var cellText = document.createTextNode(tekst);
                cell.appendChild(cellText);
                pocetak+=1;
                j++;
            }
    
            else{
              pocetak+=0.5;
            }
          }
          else if(j==0){
            var cellText = document.createTextNode(dani[i-1]);
            cell.appendChild(cellText);
            cell.classList.add("dan");
          }
          row.appendChild(cell);
        }
        tbl.appendChild(row);
      }
      div.appendChild(tbl);
      }
    }
    
    var dodajAktivnost = function(raspored, naziv, tip, vrijemePocetak, vrijemeKraj, dan){
      if(raspored!=null)
      var x = raspored.querySelectorAll("table.tg");
  
      if(raspored==null || x.length==0 ){
        return "Greška - raspored nije kreiran";
      }
  
      var pocetak=-1;
      if(x[0].rows[0].cells[0].innerHTML != "")
        pocetak= parseInt(x[0].rows[0].cells[0].innerHTML);
      else if(x[0].rows[0].cells[2].innerHTML != ""){
        pocetak= parseInt(x[0].rows[0].cells[2].innerHTML)-1;
      }
      else {
        pocetak= parseInt(x[0].rows[0].cells[4].innerHTML)-2;
      }
  
      var n = x[0].rows[1].length;
      var kraj = (n-1)/2 + pocetak;
      var dani= [];
    
      for(var i=1; i<x[0].rows.length; i++){
        dani.push(x[0].rows[i].cells[0].innerHTML);
      }
  
      if(vrijemeKraj<vrijemePocetak || vrijemePocetak%0.5!=0 || vrijemeKraj%0.5!=0 || vrijemeKraj > 24 || vrijemePocetak<0 || pocetak==-1 || vrijemePocetak<pocetak || vrijemeKraj> kraj || !dani.includes(dan)){
        return "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin";
      }
  
      var i = dani.indexOf(dan)+1;
      var j = (vrijemePocetak - pocetak)*2+1;
      var col = (vrijemeKraj-vrijemePocetak)*2;
  
      for(var l=1; l<=j; l++){
        var celija = x[0].rows[i].cells[l];
        if(celija.innerHTML != ""){
          for(var p = l; p<l + celija.colSpan; p++)
            if(p == j) {
              return "Greška - već postoji termin u rasporedu u zadanom vremenu";
            }
        }
      }
  
      for(var l = j; l< j+ col ; l++){
        if(x[0].rows[i].cells[l].innerHTML != ""){
            return "Greška - već postoji termin u rasporedu u zadanom vremenu";
        }
      }
      
      var cell = x[0].rows[i].cells[j];
      cell.colSpan = col;
      if((vrijemePocetak*2)%2!=0) cell.classList.add("tg-das2");
      if((vrijemeKraj*2)%2!=0) cell.classList.add("tg-das1");
      if((vrijemeKraj*2)%2==0 && (vrijemePocetak*2)%2==0)cell.classList.add("tg-das");
      var cellText = document.createTextNode(naziv);
      var span = document.createElement("span");
      span.innerHTML = tip;
      cell.appendChild(cellText);
      cell.appendChild(span);
    
      for(var m = j+1; m< j+col; m++){
        x[0].rows[i].cells[m].style.display = "none";
      }
  
      return "";
    }
    return {
        iscrtajRaspored : iscrtajRaspored,
        dodajAktivnost:dodajAktivnost
    }
}());