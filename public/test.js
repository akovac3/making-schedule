let assert = chai.assert;
describe('Raspored', function() {
 describe('iscrtajRaspored()', function() {
   it('Raspored utorak, srijeda, cetvrtak 7-14', function() {
     let div = document.getElementById("test");
     Raspored.iscrtajRaspored(div, ["Utorak", "Srijeda", "Četvrtak"], 7, 14);
     let tabels = document.getElementsByTagName("table");
     let table = tabels[tabels.length-1];
     let rows = table.getElementsByTagName("tr");
     assert.equal(table.classList[0], "tg", "Klasa tabele je tg");
     assert.equal(rows.length, 4,"Broj redova treba biti 4");
     assert.equal(rows[1].cells.length, 15, "Broj kolona treba biti 15");
     assert.equal(rows[0].cells[2].innerText, "08:00", "Tekst celije treba biti 08:00");
     assert.equal(rows[0].cells[8].innerText, "12:00", "Tekst celije treba biti 12:00");
     assert.equal(rows[1].cells[0].innerText, "Utorak", "Tekst celije treba biti Utorak");
     assert.equal(rows[3].cells[0].innerText, "Četvrtak", "Tekst celije treba biti Četvrtak");
     div.innerHTML="";
   });

   it('Raspored ponedjeljak, utorak, srijeda, cetvrtak, petak 8-21', function() {
     let div = document.getElementById("test");
     Raspored.iscrtajRaspored(div, ["Ponedjeljak","Utorak", "Srijeda", "Četvrtak", "Petak"], 8, 21);
     let tabels = document.getElementsByTagName("table");
     let table = tabels[tabels.length-1];
     let rows = table.getElementsByTagName("tr");
     assert.equal(rows.length, 6,"Broj redova treba biti 6");
     assert.equal(rows[1].cells.length, 27, "Broj kolona treba biti 27");
     assert.equal(rows[0].cells[0].innerText, "08:00", "Tekst celije treba biti 08:00");
     assert.equal(rows[0].cells[17].innerText, "19:00", "Tekst celije treba biti 19:00");
     assert.equal(rows[1].cells[0].innerText, "Ponedjeljak", "Tekst celije treba biti Ponedjeljak");
     assert.equal(rows[5].cells[0].innerText, "Petak", "Tekst celije treba biti Petak");
     div.innerHTML="";
   });

   it('Raspored ponedjeljak, 0-1', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Ponedjeljak"], 0, 1);
    let tabels = document.getElementsByTagName("table");
    let table = tabels[tabels.length-1];
    let rows = table.getElementsByTagName("tr");
    assert.equal(rows.length, 2,"Broj redova treba biti 2");
    assert.equal(rows[1].cells.length, 3, "Broj kolona treba biti 3");
    assert.equal(rows[0].cells[0].innerText, "00:00", "Tekst celije treba biti 00:00");
    assert.equal(rows[1].cells[0].classList[0], "dan", "Klasa vremena je dan");
    assert.equal(rows[0].cells[1].innerText, "", "Tekst celije treba biti prazan");
    assert.equal(rows[1].cells[0].innerText, "Ponedjeljak", "Tekst celije treba biti Ponedjeljak");
    div.innerHTML="";
  });

  it('Raspored srijeda petak, 0-2', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Srijeda", "Petak"], 0, 2);
    let tabels = document.getElementsByTagName("table");
    let table = tabels[tabels.length-1];
    let rows = table.getElementsByTagName("tr");
    assert.equal(rows.length, 3,"Broj redova treba biti 3");
    assert.equal(rows[1].cells.length, 5, "Broj kolona treba biti 5");
    assert.equal(rows[0].cells[0].innerText, "00:00", "Tekst celije treba biti 00:00");
    assert.equal(rows[1].cells[0].innerText, "Srijeda", "Tekst celije treba biti Srijeda");
    assert.equal(rows[2].cells[0].innerText, "Petak", "Tekst celije treba biti Petak");
    div.innerHTML="";
  });

  it('Raspored bez dana', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, [], 12, 17);
    let tabels = document.getElementsByTagName("table");
    let table = tabels[tabels.length-1];
    let rows = table.getElementsByTagName("tr");
    assert.equal(rows.length, 1,"Broj redova treba biti 1");
    assert.equal(rows[0].cells[0].classList[0], "sat", "Klasa vremena je sat");
    assert.equal(rows[0].cells[0].innerText, "12:00", "Tekst celije treba biti 12:00");
    assert.equal(rows[0].cells[5].innerText, "15:00", "Tekst celije treba biti 15:00");
    div.innerHTML="";
  });

  it('Raspored svi dani od 0 do 24', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Ponedjeljak","Utorak", "Srijeda", "Četvrtak", "Petak"], 0, 24);
    let tabels = document.getElementsByTagName("table");
    let table = tabels[tabels.length-1];
    let rows = table.getElementsByTagName("tr");
    assert.equal(rows.length, 6,"Broj redova treba biti 6");
    assert.equal(rows[1].cells.length, 49, "Broj kolona treba biti 49");
    assert.equal(rows[0].cells[0].innerText, "00:00", "Tekst celije treba biti 00:00");
    assert.equal(rows[0].cells[3].innerText, "02:00", "Tekst celije treba biti 02:00");
    assert.equal(rows[0].cells[9].innerText, "06:00", "Tekst celije treba biti 06:00");
    assert.equal(rows[3].cells[0].innerText, "Srijeda", "Tekst celije treba biti Srijeda");
    assert.equal(rows[5].cells[0].innerText, "Petak", "Tekst celije treba biti Petak");
    div.innerHTML="";
  });

    it('Greska satPocetak nije cijeli broj', function() {
      let div = document.getElementById("test");
      Raspored.iscrtajRaspored(div, ["Ponedjeljak", "Utorak", "Četvrtak"], 7.5, 14);
      let tekst = div.innerHTML;
      assert.equal(tekst, "Greška","Treba biti greška");
      div.innerHTML="";
    });

    it('Greska satPocetak nije manji od satKraj', function() {
      let div = document.getElementById("test");
      Raspored.iscrtajRaspored(div, ["Utorak", "Srijeda", "Četvrtak"], 12, 12);
      let tekst = div.innerHTML;
      assert.equal(tekst, "Greška","Treba biti greška");
      div.innerHTML="";
    });

    it('Greska satPocetak manji od 0', function() {
      let div = document.getElementById("test");
      Raspored.iscrtajRaspored(div, ["Ponedjeljak", "Utorak"], -1, 8);
      let tekst = div.innerHTML;
      assert.equal(tekst, "Greška","Treba biti greška");
      div.innerHTML="";
    });

    it('Greska satKraj veći od 24', function() {
      let div = document.getElementById("test");
      Raspored.iscrtajRaspored(div, ["Srijeda", "Petak"], 12, 25);
      let tekst = div.innerHTML;
      assert.equal(tekst, "Greška","Treba biti greška");
      div.innerHTML="";
    });
 });
});

describe('Raspored', function() {
 describe('dodajAktivnost()', function() {
   it('Više aktivnosti u različite dane', function() {
     let div = document.getElementById("test");
     Raspored.iscrtajRaspored(div, ["Utorak", "Srijeda", "Četvrtak"], 7, 14);
     Raspored.dodajAktivnost(div, "WT", "predavanje", 8, 10.5, "Utorak");
     Raspored.dodajAktivnost(div, "WT", "vježbe", 8, 10.5, "Srijeda");
     Raspored.dodajAktivnost(div, "RMA", "predavanje", 10, 12, "Četvrtak");
     let tabele = document.getElementsByTagName("table");
     let tabela = tabele[tabele.length-1];
     let redovi = tabela.getElementsByTagName("tr");
     assert.equal(redovi.length, 4,"Broj redova treba biti 4");
     assert.equal(redovi[1].cells[3].colSpan, 5, "colSpan treba biti 5");
     assert.equal(redovi[1].cells[3].innerText, "WTpredavanje", "Tekst celije treba biti WT predavanje");
     assert.equal(redovi[2].cells[3].innerText, "WTvježbe", "Tekst celije treba biti WT vježbe");
     assert.equal(redovi[3].cells[7].innerText, "RMApredavanje", "Tekst celije treba biti RMA predavanje");
     assert.equal(redovi[1].cells[0].classList[0], "dan", "Klasa dana je dan");
     assert.equal(redovi[1].cells[3].classList[0], "tg-das1", "Desni border-dashed");
     div.innerHTML="";
   });

   it('Dodavanje više aktivnosti jedna iza druge', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Utorak", "Srijeda", "Četvrtak"], 7, 15);
    Raspored.dodajAktivnost(div, "WT", "predavanje", 8, 10.5, "Utorak");
    Raspored.dodajAktivnost(div, "WT", "vježbe", 10.5, 13.5, "Utorak");
    Raspored.dodajAktivnost(div, "RMA", "predavanje", 13.5, 15, "Utorak");
    let tabele = document.getElementsByTagName("table");
    let tabela = tabele[tabele.length-1];
    let redovi = tabela.getElementsByTagName("tr");
    assert.equal(redovi.length, 4,"Broj redova treba biti 4");
    assert.equal(redovi[1].cells[3].colSpan, 5, "colSpan treba biti 5");
    assert.equal(redovi[1].cells[3].innerText, "WTpredavanje", "Tekst celije treba biti WT predavnje");
    assert.equal(redovi[1].cells[8].innerText, "WTvježbe", "Tekst celije treba biti WT vježbe");
    assert.equal(redovi[1].cells[3].classList[0], "tg-das1", "Desna linija dashed-klasa");
    div.innerHTML="";
  });

  it('Dodavanje aktivnosti koja traje cijeli dan', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Srijeda", "Četvrtak"], 0, 24);
    Raspored.dodajAktivnost(div, "WT", "predavanje", 0, 24, "Srijeda");
    Raspored.dodajAktivnost(div, "OI", "vježbe", 16.5, 17, "Četvrtak");
    Raspored.dodajAktivnost(div, "RMA", "vježbe", 17, 19, "Četvrtak");
    let tabele = document.getElementsByTagName("table");
    let tabela = tabele[tabele.length-1];
    let redovi = tabela.getElementsByTagName("tr");
    assert.equal(redovi.length, 3,"Broj redova treba biti 3");
    assert.equal(redovi[1].cells[1].colSpan, 48, "colSpan treba biti 48");
    assert.equal(redovi[1].cells[1].innerText, "WTpredavanje", "Tekst celije treba biti WT predavnje");
    assert.equal(redovi[2].cells[34].innerText, "OIvježbe", "Tekst celije treba biti OI vježbe");
    assert.equal(redovi[1].cells[1].classList[0], "tg-das", "Aktivnost klasa");
    assert.equal(redovi[2].cells[34].classList[0], "tg-das2", "Lijeva linija dashed - klasa");
    div.innerHTML="";
  });

  it('Dodavanje aktivnosti koje traju po pola sata za cijeli dan', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Petak"], 12, 14);
    Raspored.dodajAktivnost(div, "RMA", "predavanje", 12, 12.5, "Petak");
    Raspored.dodajAktivnost(div, "OI", "predavanje", 12.5, 13, "Petak");
    Raspored.dodajAktivnost(div, "RMA", "vježbe", 13, 13.5, "Petak");
    Raspored.dodajAktivnost(div, "OI", "vježbe", 13.5, 14, "Petak");
    let tabele = document.getElementsByTagName("table");
    let tabela = tabele[tabele.length-1];
    let redovi = tabela.getElementsByTagName("tr");
    assert.equal(redovi.length, 2,"Broj redova treba biti 2");
    assert.equal(redovi[1].cells[1].colSpan, 1, "colSpan treba biti 1");
    assert.equal(redovi[1].cells[1].innerText, "RMApredavanje", "Tekst celije treba biti RMA predavnje");
    assert.equal(redovi[1].cells[4].innerText, "OIvježbe", "Tekst celije treba biti OI vježbe");
    assert.equal(redovi[1].cells[3].classList[0], "tg-das1", "Desni border dashed - klasa");
    assert.equal(redovi[1].cells[4].classList[0], "tg-das2", "Lijevi border dashed - klasa");
    div.innerHTML="";
  });

  

   it('Greška - dodavanje aktivnosti čiji dan nije u rasporedu', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Utorak", "Srijeda", "Četvrtak"], 7, 14);
    let povratna = Raspored.dodajAktivnost(div, "WT", "predavanje", 8, 10, "Ponedjeljak");
    assert.equal(povratna, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin", "Greška ne postoji dan");
    div.innerHTML="";
  });

  it('Greška - raspored nije kreiran', function() {
    let div = document.getElementById("test");
    let povratna = Raspored.dodajAktivnost(div, "WT", "predavanje", 0, 10, "Ponedjeljak");
    assert.equal(povratna, "Greška - raspored nije kreiran", "Greška raspored nije kreiran");
    div.innerHTML="";
  });

  it('Greška - dodavanje aktivnosti čije vrijeme nije u opsegu', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Ponedjeljak", "Srijeda", "Četvrtak"], 8, 14);
    let povratna = Raspored.dodajAktivnost(div, "WT", "vježbe", 7, 9, "Ponedjeljak");
    assert.equal(povratna, "Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin", "Greška ne postoji vrijeme");
    div.innerHTML="";
  });

  it('Greška - postoji već aktivnost koja se djelimično preklapa, dodavanje iza', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Srijeda", "Četvrtak", "Petak"], 12, 20);
    let povratna1 = Raspored.dodajAktivnost(div, "WT", "vježbe", 13.5, 15.5, "Petak");
    let povratna2 = Raspored.dodajAktivnost(div, "RMA", "vježbe", 15, 17, "Petak");
    assert.equal(povratna1, "", "Uspješno dodana prva aktivnost");
    assert.equal(povratna2, "Greška - već postoji termin u rasporedu u zadanom vremenu", "Greška već postoji termin u zadanom vremenu");
    div.innerHTML="";
  });

  it('Greška - postoji već aktivnost koja se djelimično preklapa, dodavanje ispred', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Ponedjeljak", "Četvrtak", "Petak"], 1, 11);
    let povratna1 = Raspored.dodajAktivnost(div, "WT", "predavanje", 5, 7.5, "Četvrtak");
    let povratna2 = Raspored.dodajAktivnost(div, "RMA", "vježbe", 3.5, 5.5, "Četvrtak");
    assert.equal(povratna1, "", "Uspješno dodana prva aktivnost");
    assert.equal(povratna2, "Greška - već postoji termin u rasporedu u zadanom vremenu", "Greška već postoji termin u zadanom vremenu");
    div.innerHTML="";
  });

  it('Greška - dodavanje aktivnosti koja pocinje poslije i završava prije prve', function() {
    let div = document.getElementById("test");
    Raspored.iscrtajRaspored(div, ["Utorak", "Petak"], 9, 12);
    let povratna1 = Raspored.dodajAktivnost(div, "OI", "predavanje", 9, 11, "Petak");
    let povratna2 = Raspored.dodajAktivnost(div, "OI", "vježbe", 9.5, 10.5, "Petak");
    assert.equal(povratna1, "", "Uspješno dodana prva aktivnost");
    assert.equal(povratna2, "Greška - već postoji termin u rasporedu u zadanom vremenu", "Greška već postoji termin u zadanom vremenu");
    div.innerHTML="";
  });


 });
});