var predmeti=[];
var aktivnosti=[];
window.addEventListener('load', event => {
    var predmetiHttp = new XMLHttpRequest();
    var aktivnostiHttp = new XMLHttpRequest();
    var tipHttp = new XMLHttpRequest();

    tipHttp.onreadystatechange = function() { 
        if (tipHttp.readyState == 4 && tipHttp.status == 200){
            let tipovi = JSON.parse(tipHttp.responseText)
            for(var i=0; i<tipovi.length; i++ ){
                document.getElementById("tip").innerHTML += "<option value=" + tipovi[i].id + ">" + tipovi[i].naziv + "</option>" ;
            }
        }
        else if (tipHttp.readyState == 4 && tipHttp.status == 200) {
            console.log("Dohvacanje tipova sa servera neuspješno!")
        }

        
    }
    
    predmetiHttp.onreadystatechange = function() { 
        if (predmetiHttp.readyState == 4 && predmetiHttp.status == 200){
            let predmet = JSON.parse(predmetiHttp.responseText);
            for(var i=0; i<predmet.length; i++ ){
                predmeti.push(predmet[i].naziv)
                document.getElementById("predmetiTabela").innerHTML +="<tr>" + "<td>" + predmet[i].naziv+ "</td>" + "</tr>"
            }
        }
    }

    aktivnostiHttp.onreadystatechange = function() { 
        if (aktivnostiHttp.readyState == 4 && aktivnostiHttp.status == 200){
            let aktivnost = JSON.parse(aktivnostiHttp.responseText);
            for(var i=0; i<aktivnost.length; i++ ){
                aktivnosti.push(aktivnost[i])
                document.getElementById("aktivnostiTabela").innerHTML +=
                    "<tr>"
                    + "<td>" + aktivnost[i].naziv + "</td>"
                    + "<td>" + aktivnost[i].Tip.naziv + "</td>"
                    + "<td>" + aktivnost[i].pocetak + "</td>"
                    + "<td>" + aktivnost[i].kraj + "</td>"
                    + "<td>" + aktivnost[i].Dan.naziv + "</td>" +
                    "</tr>"
            }
        }
    }
    predmetiHttp.open("GET", 'http://localhost:3000/v2/predmet', true);
    predmetiHttp.send();
  
    aktivnostiHttp.open("GET", 'http://localhost:3000/v2/aktivnost', true);
    aktivnostiHttp.send();

    tipHttp.open("GET", "http://localhost:3000/v2/tip", true)
    tipHttp.send();
});

function obrisiPredmet(predmet){
    var ajax = new XMLHttpRequest();
    ajax.open("DELETE", "http://localhost:3000/v2/predmet/"+predmet, true);
    ajax.send();
}

function dodajPredmet(predmet, tipId, tip, pocetak, kraj, dan){
    var nova = JSON.stringify({"naziv" : predmet});
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function() {
	    if (ajax.readyState == 4 && (ajax.status == 200 || ajax.status == 409)){
            var ima = false;
            if(JSON.parse(ajax.response).message == "Naziv predmeta postoji!") ima = true;
            dodajDan(predmet, tipId, tip, pocetak, kraj, dan, JSON.parse(ajax.response).predmet.id, ima)
        }
        else if(ajax.readyState == 4 && ajax.status == 400){
            var x = document.getElementById("snackbar");
            x.innerHTML = "Dodavanje aktivnosti nije uspješno"
            x.className = "show";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        }
    }
    ajax.open("POST", "http://localhost:3000/v2/predmet", true);
    ajax.setRequestHeader("Content-type", "application/json");
    ajax.send(nova);
}

function dodajAktivnost(naziv, tipId, tip, pocetak, kraj, dan, id, ima){
    var nova = JSON.stringify({"naziv" : naziv.toUpperCase(), "tip" : tip, "pocetak" : pocetak, "kraj" : kraj, "dan" : dan.naziv});
    var uBazu = JSON.stringify({"naziv" : naziv.toUpperCase() + " " + tip, "TipId" : tipId, "pocetak" : pocetak, "kraj" : kraj, "DanId" : dan.id, "predmetId" : id});
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && (ajax.status == 200 || ajax.status == 409)){
            if(JSON.parse(ajax.response).message == "Aktivnost nije validna!") {
                var x = document.getElementById("snackbar");
                x.innerHTML = "Aktivnost nije validna"
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                if(!ima) obrisiPredmet(id)
            }
            else {
                aktivnosti.push(nova);
                
                document.getElementById("aktivnostiTabela").innerHTML += "<tr>"
                    + "<td>" + naziv.toUpperCase() + " " + tip + "</td>"
                    + "<td>" + tip + "</td>"
                    + "<td>" + pocetak + "</td>"
                    + "<td>" + kraj + "</td>"
                    + "<td>" + dan.naziv + "</td>" +
                    "</tr>" ;
                if(!ima) {
                    predmeti.push(JSON.stringify({id : id, naziv : naziv.toUpperCase()}));
                    document.getElementById("predmetiTabela").innerHTML +="<tr>" + "<td>" + naziv.toUpperCase() + "</td>" + "</tr>"
                }
                var x = document.getElementById("snackbar");
                x.innerHTML = "Uspješno dodana aktivnost!";
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            }
            
        }
        if (ajax.readyState == 4 && ajax.status == 400){
            if(!ima)obrisiPredmet(naziv);
            var x = document.getElementById("snackbar");
            x.innerHTML = "Dodavanje aktivnosti nije uspješno"
            x.className = "show";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        }
    }
    ajax.open("POST", "http://localhost:3000/v2/aktivnost", true);
    ajax.setRequestHeader("Content-type", "application/json");
    ajax.send(uBazu);
}

function dodajDan(predmet, tipId, tip, pocetak, kraj, dan, id, postoji){
    var novi = JSON.stringify({"naziv" : dan});
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function() {
	    if (ajax.readyState == 4 && ajax.status == 200){
            dodajAktivnost(predmet, tipId, tip, pocetak, kraj, JSON.parse(ajax.response).dan, id, postoji)
        }
        else if(ajax.readyState == 4 && ajax.status == 400){
            var x = document.getElementById("snackbar");
            x.innerHTML = "Dodavanje aktivnosti nije uspješno"
            x.className = "show";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            if(!postoji) obrisiPredmet(id);
        }
    }
    ajax.open("POST", "http://localhost:3000/v2/dan", true);
    ajax.setRequestHeader("Content-type", "application/json");
    ajax.send(novi);
}

function unesiRaspored(){
    
    var naziv = document.getElementById("naziv").value;

    if(naziv==""){
        var x = document.getElementById("snackbar");
        x.innerHTML = "Dodavanje aktivnosti nije uspješno"
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
   }
    else {
        var e= document.getElementById("tip")
        var tipId = e.options[e.selectedIndex].value;
        var tip = e.options[e.selectedIndex].text;
        var pocetak = Number(document.getElementById("vrijpoc").value.toString().replace(':', '.').replace('.3', '.5'));
        var kraj = Number(document.getElementById("vrijkraj").value.toString().replace(':', '.').replace('.3', '.5'));
        var f = document.getElementById("dani");
        var dan = f.options[f.selectedIndex].text;

        dodajPredmet(naziv, tipId, tip, pocetak, kraj, dan)
    }
}