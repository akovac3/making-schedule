window.addEventListener('load', event => {
    var studentiHttp = new XMLHttpRequest();

    studentiHttp.onreadystatechange = function() { 
        if (studentiHttp.readyState == 4 && studentiHttp.status == 200){
            let grupe = JSON.parse(studentiHttp.responseText)
            for(var i=0; i<grupe.length; i++ ){
                document.getElementById("grupe").innerHTML += "<option value=" + grupe[i].id + ">" + grupe[i].naziv + "</option>" ;
            }
        }
        else if (studentiHttp.readyState == 4 && studentiHttp.status == 200) {
            console.log("Dohvaćanje grupa sa servera nije uspjelo")
        }

        
    }
    
    studentiHttp.open("GET", "http://localhost:3000/v2/grupa", true)
    studentiHttp.send();
})

function dohvatiPodatke(){
    var redovi = document.getElementById("unos").value.split('\n');
    var studenti = [];
    for(var red of redovi){
        var jedan = red.split(',');
        if(jedan.length !=2){
            document.getElementById("unos").value="Pogresan CSV format";
            return false;
        }
        studenti.push({
            ime : jedan[0],
            index : jedan[1],
            grupaId : Number(document.getElementById("grupe").value)
        })
    }
    return studenti;
}

function posalji(){
    if(Number(document.getElementById("grupe").value) != 0){
        var studenti = dohvatiPodatke();
        if(studenti){
            var studentiHttp = new XMLHttpRequest();

            studentiHttp.onreadystatechange = function() { 
                if (studentiHttp.readyState == 4 && studentiHttp.status == 200){
                    var niz = JSON.parse(studentiHttp.response).message;
                    document.getElementById("unos").value = ""
                    for(var i=0; i<niz.length; i++)
                        document.getElementById("unos").value += niz[i] + "\n";
                    
                }
                else if (studentiHttp.readyState == 4 && studentiHttp.status == 404) {
                    console.log("Spašavanje u bazu nije uspjelo")
                }

        
            }
            studentiHttp.open("POST", "http://localhost:3000/v2/studenti", true)
            studentiHttp.setRequestHeader("Content-type", "application/json;charset=UTF-8")
            studentiHttp.send(JSON.stringify(studenti));
        }
    }
}