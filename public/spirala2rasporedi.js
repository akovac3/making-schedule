window.onload = function () {
    let div = document.getElementById("tabela1");
    iscrtajRaspored(div, ["Utorak", "Srijeda", "Četvrtak"], 7, 15);
    let div2 = document.getElementById("tabela2");
    iscrtajRaspored(div2, ["Ponedjeljak","Utorak", "Srijeda", "Četvrtak", "Petak"], 8, 21);
    
    var poziv1 = dodajAktivnost(div, "WT", "predavanje", 8, 11, "Srijeda");
    if(poziv1!= "") alert(poziv1);
    var poziv2 =dodajAktivnost(div, "RMA", "predavanje", 11, 13, "Utorak");
    if(poziv2!= "") alert(poziv2);
    var poziv3 =dodajAktivnost(div, "RMA", "vježbe", 8, 10, "Četvrtak");
    if(poziv3!= "") alert(poziv3);
    var poziv4 =dodajAktivnost(div, "RMA", "vježbe", 9.5, 11, "Ponedjeljak");
    if(poziv4!= "") alert(poziv4);
    var poziv5 =dodajAktivnost(div, "WT", "vježbe", 10.5, 13.5, "Četvrtak");
    if(poziv5!= "") alert(poziv5);
    var poziv6 = dodajAktivnost(div,"DM","predavanje",10,11.5,"Utorak");
    if(poziv6!= "") alert(poziv6);
    var poziv7 = dodajAktivnost(div,"DM","predavanje",7,8.5,"Utorak");
    if(poziv7!= "") alert(poziv7);
    var poziv8 = dodajAktivnost(div,"OOI","predavanje",12,13.5,"Srijeda");
    if(poziv8!= "") alert(poziv8);
    var poziv9 = dodajAktivnost(div,"OOI","vježbe",9.5,10.5,"Utorak");
    if(poziv9!= "") alert(poziv9);
    var poziv10 = dodajAktivnost(div,"DM","vježbe",14,15,"Utorak");
    if(poziv10!= "") alert(poziv10);


    var poziv11 = dodajAktivnost(div2,"DM","predavanje",13,15.5,"Utorak");
    if(poziv11!= "") alert(poziv11);
    var poziv12 = dodajAktivnost(div2,"OI","predavanje",13,15,"Četvrtak");
    if(poziv12!= "") alert(poziv12);
    var poziv13 = dodajAktivnost(div2, "WT", "predavanje", 10, 12.5, "Ponedjeljak");
    if(poziv13!= "") alert(poziv13);
    var poziv14 = dodajAktivnost(div2, "WT", "vježbe", 9, 11, "Ponedjeljak");
    if(poziv14!= "") alert(poziv14);
    var poziv15 = dodajAktivnost(div2, "RMA", "predavanje", 9, 12, "Srijeda");
    if(poziv15!= "") alert(poziv15);
    var poziv16 = dodajAktivnost(div2, "RMA", "vježbe", 17, 18, "Srijeda");
    if(poziv16!= "") alert(poziv16);
    var poziv17 = dodajAktivnost(div2, "WT", "vežbe", 18, 20, "Četvrtak");
    if(poziv17!= "") alert(poziv17);
    var poziv18 = dodajAktivnost(div2, "DM", "vježbe", 11.5, 13, "Petak");
    if(poziv18!= "") alert(poziv18);
    var poziv19 = dodajAktivnost(div2, "OI", "vježbe", 13, 14.5, "Petak");
    if(poziv19!= "") alert(poziv19);
    var poziv20 = dodajAktivnost(div2, "IM1", "vježbe", 16, 19, "Utorak");
    if(poziv20!= "") alert(poziv20);

    let div3 = document.getElementById("tabela3");
    iscrtajRaspored(div3, ["Ponedjeljak", "Utorak", "Četvrtak"], -1, 18);
    var poziv21 = dodajAktivnost(div3, "WT", "predavanje", 8, 11, "Utorak");
    if(poziv21!= "") alert(poziv21);

}